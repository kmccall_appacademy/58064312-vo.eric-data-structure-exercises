# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  sorted_array = arr.sort
  sorted_array[-1] - sorted_array[0]
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  return arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
$vowels = "aeiouAEIOU".chars
def num_vowels(str)
  count = 0
  for i in 0...str.length
    if $vowels.index(str[i]) != nil
      count += 1
    end
  end
  return count
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  new_str = ""
  for i in 0...str.length
    if $vowels.index(str[i]) == nil
      new_str << str[i]
    end
  end
  return new_str
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  int.to_s.chars.sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  split_string = str.downcase.chars
  for i in 0...split_string.length
    if split_string[i] == split_string[i + 1]
      return true
    end
  end
  return false
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  phone_string = arr.join("")
  return "(#{phone_string[0..2]}) #{phone_string[3..5]}-#{phone_string[6..-1]}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  sorted_array = str.split(",").sort
  return sorted_array[-1].to_i - sorted_array[0].to_i
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  rotate_position = offset % arr.length
  return arr.drop(rotate_position) + arr.take(rotate_position)
end
